# get all posts from r/kde, make a dataset for text classification.
import requests
# import pandas as pd
import csv
import time
import json

cats = ["fluff", "events", "question ", "news-releases",
        "suggestions ", "bugs", "community-content", "apps"]
categories = ["Akademy", "News", "Community Content",
              "Fluff", "General Bug", "Tip", "Suggestion", "Question"]

endtime = int(time.time())

def get_tweets(endtime):
    data = []
    # f = open("basic-analytics/text-classification/historical-data/redd.csv", "w")
    # f.write("text,title,category,up-down-votes\n")
    for i in range(100):
        startime = endtime - 86400
        url = "https://api.pushshift.io/reddit/search/submission/?subreddit=kde&sort=desc&sort_type=created_utc&after={}&before={}&size={}".format(
            startime, endtime, 1000)
        print(url)
        res = requests.get(url)

        if (res.status_code != 200):
            with open("basic-analytics/text-classification/historical-data/reddpp.json", "w") as fpt:
                json.dump(data, fpt)
            # f.close() 
            print("Returned COde {} {}",res.status_code, res.text)
            exit()

        res = res.json()["data"]

        for j in res:
            try:
                if j["link_flair_richtext"][0]["t"] in categories:
                    d = {}
                    d["text"] = j["selftext"]
                    d["title"] = j["title"]
                    d["category"] = j["link_flair_richtext"][0]["t"]
                    d["up-down-votes"] = j["score"]
                    data.append(d)
                    # f.write(repr("{},{},{},{}".format(d["text"], d["title"],d["category"] ,d["up-down-votes"])))
                    # f.write("\n")
            except Exception:
                pass
        endtime = endtime - 86400
        if (i % 10 == 0):
            time.sleep(5)
            print("sleeping")

    with open("basic-analytics/text-classification/historical-data/redd5.json", "w") as fpt:
        json.dump(data, fpt)
    # f.close()

if __name__ == "__main__":
    get_tweets(1579110081)
