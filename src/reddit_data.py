import requests
import json
import os
import logging
from pathlib import Path
import praw
from decouple import config

parent_dir = Path(__file__).parent.parent.absolute()


def subreddit_stats(sub_name, date_dir):
    # from subredditstats.com, https://subredditstats.com/api/subreddit?name={}
    url = "https://subredditstats.com/api/subreddit?name={}".format(sub_name)
    resp = requests.get(url)

    if (resp.status_code == 200):
        logging.info("GET REDDIT")
        resp = resp.json()
        try:
            f = open(Path.joinpath(
                parent_dir, "metadata/reddit_config.json"), "r")
            fields = json.loads(f.read())
            fields = fields["fields"]
            f.close()
            data = {}
            for i in fields:
                if (isinstance(resp[i], list)):
                    data[i] = resp[i][:15]
                else:
                    data[i] = resp[i]
            fp = open(Path.joinpath(date_dir, "reddit/{}_reddit_stats.json".format(sub_name)), "w")
            json.dump(data, fp)
            fp.close()
            logging.info("WRITE STATS TO FILE")
        except Exception as e:
            logging.error("FAILED TO WRITE TO FILE", exc_info=True)
    else:
        logging.error("FAILED TO GET FROM SUBREDDITSTATS.COM")


def keyword_search(date_dir):
    #https://www.reddit.com/wiki/search#wiki_field_search
    try:
        reddit = praw.Reddit(user_agent="pRomO Bot", client_id=config("REDDIT_CLIENT_ID"), client_secret=config(
            "REDDIT_CLIENT_SECRET"), password=config("REDDIT_PASSWORD"), username=config("REDDIT_USERNAME"))
        all_s = reddit.subreddit("all")
        f = open(Path.joinpath(parent_dir, "metadata/reddit_config.json"), "r")
        fields = json.loads(f.read())
        f.close()
        data = {}
        for i in fields["keyword_search"]:
            results = all_s.search(sort=fields["sort"],limit=fields["max_posts"],time_filter=fields["time_filter"],query="{} AND nsfw:no".format(i) )
            d = []
            for j in results:
                a ={}
                a["subreddit"] = str(j.subreddit)
                a["title"] = str(j.title)
                a["id"] = str(j.id)
                a["author"] = str(j.author)
                d.append(a)
            data[i] = d 
        with open(Path.joinpath(date_dir, "reddit/reddit_keyword.json"),"w") as fptr:
            json.dump(data,fptr)
    except Exception as e:
        logging.error("Exception", exc_info=True)


def main(date_dir):
    try:
        path = Path.joinpath(parent_dir, "logs/logs.log")
        logging.basicConfig(filename=path, filemode="a",
                            format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    except FileNotFoundError:
        print("Log file not found")
    try:
        f = open(Path.joinpath(parent_dir, "metadata/reddit_config.json"), "r")
        fields = json.loads(f.read())
        subreddits = fields["subreddit_names"]
        for i in subreddits:
            subreddit_stats(i, date_dir)
        f.close()
        keyword_search(date_dir)
    except Exception as e:
        logging.error("Exception", exc_info=True)


# if __name__ == "__main__":
#     try:
#         path = Path.joinpath(parent_dir, "logs/logs.log")
#         logging.basicConfig(filename=path, filemode="a",
#                             format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
#     except FileNotFoundError:
#         print("Log file not found")
#     try:
#         main()
#     except Exception as e:
#         logging.error("Exception", exc_info=True)
