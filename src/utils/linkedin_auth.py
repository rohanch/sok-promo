import requests
from decouple import config
import time
import logging
from pathlib import Path
import os

refresh_token = config("likedin_refresh_token")
access_token = config("linkedin_access_token")
parent = Path(__file__).parent.parent.parent.absolute()
env_file = Path.joinpath(parent, ".env")

def check_if_access_token_valid(atoken):
    url = "https://www.linkedin.com/oauth/v2/introspectToken"
    payload='client_id={}&client_secret={}&token={}'.format(config('linkedin_client_id'), config('linkedin_client_secret'), atoken)
    headers = {
	'Content-Type': 'application/x-www-form-urlencoded'
    }
    response = requests.request("POST", url, headers=headers, data=payload)

    if response.status_code != 200:
        raise Exception("Linkedin access token checkig returned error", response.status_code, response.text)
    response = response.json()

    time_now = int(time.time())
    expires = response["expires_at"]
    if expires - time_now > 100:
        return True
    else:
        return False

def get_new_access_token(rtoken):
    url = "https://www.linkedin.com/oauth/v2/accessToken"
    payload='client_id={}&client_secret={}&token={}'.format(config('linkedin_client_id'), config('linkedin_client_secret'), rtoken)
    headers = {
	'Content-Type': 'application/x-www-form-urlencoded'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    if response.status_code != 200:
        raise Exception("Linkedin access token getting returned error", response.status_code, response.text)
    resp = response.json()
    return resp["access_token"]


def main():
    try:
        if (not check_if_access_token_valid(refresh_token)):
            logging.error("LINKEDIN REFRESH TOKEN EXPIRED PLEASE LOGIN AGAIN AND REPLACE THE CODE")
            return False
        if (not check_if_access_token_valid(access_token)):
            new_access_token = get_new_access_token(refresh_token)
        else:
            print("returned true")
            return True
        #change access token here
        f = open(env_file, 'r+')
        c = f.read()
        pos1 = c.find('linkedin_access_token=')
        pos1 = pos1 + len('linkedin_access_token=')
        pos2 = c.find('likedin_refresh_token=')
        pos2 -=1
        dat = c[0:pos1] + new_access_token+c[pos2:]

        f.seek(0)
        f.write(dat)
        f.truncate()
        f.close()

        return True
    except Exception as e:
        logging.error("linkedin-access token retreiving failed", exc_info=True)
        return False
    

if __name__ == '__main__':
    print(main())