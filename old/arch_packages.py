import requests
import os
from bs4 import BeautifulSoup
import pickle
import datetime
import json

def get_names():
    #one time use, scrape kde website to get package names
    URL = 'https://apps.kde.org/'

    resp = requests.get(URL)

    soup = BeautifulSoup(resp.content, 'html.parser')

    div = soup.findAll("div", class_="p-3 h-100")

    names = []

    for i in div:
        t = i.findAll('a')[1].text
        t = t.strip()
        names.append(t)
    return names




def read_file():
    #one time use, get names of packages and store in pickle file
    if (not os.path.isfile("metadata/archpackages.txt")):
        f = open("metadata/archpackages.txt","w")
        names = get_names()

        final = []

        for i in names:
            url = "https://pkgstats.archlinux.de/api/packages/{}?startMonth=201901&endMonth=202001".format(i)
            resp = requests.get(url)
            if (resp.status_code == 200):
                print(resp.json())
                final.append(i)
            else:
                print(resp.status_code, i)
        
        for item in final:
            f.write("%s\n" % item)
        f.close()
    
    if (not os.path.isfile("metadata/packages.pyc")):
        names = []

        f = open("metadata/archpackages.txt","r")
        cont = f.read()
        cont = list(cont.split("\n"))

        with open('metadata/packages.pyc', 'wb') as fp:
            pickle.dump(cont, fp)


def get_arch():
    #arch linux packages stats
    packages = ["kirigami2","kirigami-gallery","KArchive","ThreadWeaver","breeze-icons","KI18n"]
    packages_query = ["plasma","kconfig","kwin"]
    

    with open ('metadata/packages.pyc', 'rb') as fp:
        itemlist = pickle.load(fp)
        fp.close()
    responses = []
    #gives results month by month 
    startmonth = datetime.datetime.now().strftime("%G") + datetime.datetime.now().strftime("%m")
    print("Making package requests ... ")
    for i in itemlist:
        url = "https://pkgstats.archlinux.de/api/packages/{}?startMonth={}&endMonth={}".format(i,startmonth,startmonth)
        res = requests.get(url)
        if(res.status_code != 200):
            print("Error {} {}",res.status_code, res.text)
            break
        responses.append(res.json())

    print("Making package requests ... ")
    for i in packages:
        url = "https://pkgstats.archlinux.de/api/packages/{}?startMonth={}&endMonth={}".format(i,startmonth,startmonth)
        res = requests.get(url)
        if(res.status_code != 200):
            print("Error {} {}",res.status_code, res.text)
            break
        responses.append(res.json())
    print("Making batch requests ... ")
    for i in packages_query:
        url = "https://pkgstats.archlinux.de/api/packages?limit=150&offset=0&query={}".format(i)
        res = requests.get(url)
        if(res.status_code != 200):
            print("Error {} {}",res.status_code, res.text)
            break
        res = res.json()
        res = res["packagePopularities"]
        responses += res
    
    with open("data/arch.json", "w") as fptr:
        json.dump(responses,fptr)

    fptr.close()

def get_ubuntu():
    #TODO
    pass    

if __name__ == "__main__":
    read_file()

    get_arch()
    