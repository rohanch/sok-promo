from decouple import config
import requests
import logging
import time
import json
from pathlib import Path

parent_dir = Path(__file__).parent.parent.absolute()

def parse_data1(inp):
    #linkedin_followers_stats.json
    mdata = open(Path.joinpath(parent_dir,"metadata/linkedin-standardised-data.json"),"r")
    mdata = json.loads(mdata.read())
    cols = [['followerCountsByRegion','region'], ['followerCountsBySeniority','seniority'], ['followerCountsByIndustry','industry'], ['followerCountsByFunction','function'], ['followerCountsByCountry','country']]
    for i in cols:
        dat = inp[i[0]]
        for j in dat:
            k = j
            try:
                if (i[1]== 'region' or i[1] == "country"):
                    val = next( item["name"]["value"] for item in mdata[i[1]] if item['$URN'] == j[i[1]])
                else:
                    val = next(item['name']['localized']['en_US'] for item in mdata[i[1]] if item["$URN"] == j[i[1]])
            except Exception as e:
                logging.error("Error here {}".format(j), exc_info=True)
                val = "NA"
            j[i[1]] = val
            dat[dat.index(k)] = j
        inp[i[0]] = dat
    # json.dumps(inp, indent=4)
    return inp
    
def parse_data2(input):
    mdata = open(Path.joinpath(parent_dir,"metadata/linkedin-standardised-data.json"),"r")
    mdata = json.loads(mdata.read())
    cols = [['pageStatisticsBySeniority','seniority'], ['pageStatisticsByCountry','country'], ['pageStatisticsByIndustry','industry'], ['pageStatisticsByRegion','region'],['pageStatisticsByFunction','function']]
    for i in cols:
        dat = input[i[0]]
        for j in dat:
            k = j
            try:
                if (i[1]== 'region' or i[1] == "country"):
                    val = next( item["name"]["value"] for item in mdata[i[1]] if item['$URN'] == j[i[1]])
                else:
                    val = next(item['name']['localized']['en_US'] for item in mdata[i[1]] if item["$URN"] == j[i[1]])
            except Exception as e:
                logging.error("Error here {}".format(j), exc_info=True)
                val = "NA"
            j[i[1]] = val
            dat[dat.index(k)] = j
        input[i[0]] = dat
    return input


def get_stats(date_dir):
    try:
        headers = {
            'Authorization': 'Bearer {}'.format(config("linkedin_access_token"))
        }
        #no parse
        try:
            url = "https://api.linkedin.com/v2/networkSizes/urn:li:organization:{}?edgeType=CompanyFollowedByMember".format(config("LINKEDIN_KDE_ORGID"))
            response = requests.request("GET", url, headers=headers)

            if (response.status_code != 200):
                raise Exception("Exception {} {}".format(response.status_code,response.text))    
        except Exception as e:
            logging.error("Followers Gave the error", exc_info=True)

        data = {}
        data["followerCount"]=response.json()["firstDegreeSize"]

        try:
            url = "https://api.linkedin.com/v2/organizationalEntityShareStatistics?q=organizationalEntity&organizationalEntity=urn:li:organization:{}".format(config("LINKEDIN_KDE_ORGID"))
            res = requests.get(url,headers=headers)

            if (res.status_code != 200):
                raise Exception("Exception {} {}".format(res.status_code, res.text))

            data.update(res.json()["elements"][0]['totalShareStatistics'])
        except Exception as e:
            logging.error("Error", exc_info=True)         

        # write to file
        #no parse
        with open(Path.joinpath(date_dir, "linkedin/linkedin_stats.json"),"w") as fpt:
            json.dump(data, fpt)

        #   WEEKLY FOLLOWER GAINS :: STATS
        data = {}

        end_time = int(time.time()*1000)
        start_time = int(time.time()) - 604810
        start_time = start_time*1000
        try:
            url = "https://api.linkedin.com/v2/organizationalEntityFollowerStatistics?q=organizationalEntity&organizationalEntity=urn:li:organization:{}&timeIntervals.timeGranularityType=DAY&timeIntervals.timeRange.start={}&timeIntervals.timeRange.end={}".format(config("LINKEDIN_KDE_ORGID"),start_time,end_time)
            res = requests.get(url, headers=headers)

            if (res.status_code != 200):
                raise Exception("Exception {} {}".format(res.status_code, res.text))
            data = res.json()["elements"]
            #no parse needed
            with open(Path.joinpath(date_dir, "linkedin/linkedin_weekly_followers.json"),"w") as fpt:
                json.dump(data, fpt)
        except Exception as e:
            logging.error("Error",exc_info=True)

        #   LIFETIME FOLLOWER DEMOGRAPHIC DATA, CHECK METADATA FOR REFERENCE
        data = {}

        try:
            url = "https://api.linkedin.com/v2/organizationalEntityFollowerStatistics?q=organizationalEntity&organizationalEntity=urn:li:organization:{}".format(config("LINKEDIN_KDE_ORGID"))
            res = requests.get(url,headers=headers)
            if (res.status_code != 200):
                raise Exception("Exception {} {}".format(res.status_code, res.text))
            data = res.json()["elements"][0]
            #to parse
            with open(Path.joinpath(date_dir, "linkedin/linkedin_followers_stats.json"),"w") as fpt:
                dt = parse_data1(data)
                json.dump(dt, fpt)
        except Exception as e:
            logging.error("Error",exc_info=True)

        #   LIFETIME PAGE VIEWS
        data = {}

        try:
            url = "https://api.linkedin.com/v2/organizationPageStatistics?q=organization&organization=urn:li:organization:{}".format(config("LINKEDIN_KDE_ORGID"))
            res = requests.get(url,headers=headers)
            if (res.status_code != 200):
                raise Exception("Exception {} {}".format(res.status_code, res.text))
            data = res.json()["elements"][0]
            #to parse
            with open(Path.joinpath(date_dir, "linkedin/linkedin_page_stats.json"),"w") as fpt:
                json.dump(parse_data2(data), fpt)
        except Exception as e:
            logging.error("Error",exc_info=True)
        
        try:
            url = "https://api.linkedin.com/v2/organizationPageStatistics?q=organization&organization=urn:li:organization:{}&timeIntervals.timeGranularityType=DAY&timeIntervals.timeRange.start={}&timeIntervals.timeRange.end={}".format(config("LINKEDIN_KDE_ORGID"),start_time,end_time)
            res = requests.get(url,headers=headers)
            if (res.status_code != 200):
                raise Exception("Exception {} {}".format(res.status_code, res.text))
            data = res.json()["elements"][0]
            #no parse needed
            with open(Path.joinpath(date_dir, "linkedin/linkedin_weekly_page_stats.json"),"w") as fpt:
                json.dump(data, fpt)
        except Exception as e:
            logging.error("Error",exc_info=True)
    except Exception as e:
        logging.error("Exception", exc_info=True)


def posts_stats(date_dir):
    try:
        headers={'Authorization': 'Bearer {}'.format(config("linkedin_access_token"))}
        res = requests.get("https://api.linkedin.com/v2/shares?count=75&owners=urn%3Ali%3Aorganization%3A29561&q=owners&sharesPerOwner=1000&sortBy=CREATED", headers=headers)
        # print(res.json())
        if res.status_code != 200:
            raise Exception("Exception {} {}".format(res.status_code, res.text))
        res = res.json()["elements"]
        # print(res)
        # print(len(res))
        # exit()
        # print("test")
        ids = [x["id"] for x in res]

        config_file = open(Path.joinpath(parent_dir, "metadata/linkedin-config.json"),"r+")
        fields = json.loads(config_file.read())
        last = fields["lastRetreived"]
        

        try:
            ids = ids[0:ids.index(last)]
        except ValueError:
            pass
        url = "https://api.linkedin.com/v2/organizationalEntityShareStatistics?q=organizationalEntity&organizationalEntity=urn:li:organization:29561&"
        for i in range(0, len(ids)):
            url += "shares[{}]=urn:li:share:{}&".format(i, ids[i])

        res2 = requests.get(url, headers=headers)
        if res2.status_code != 200:
            raise Exception("Exception {} {}".format(res2.status_code, res2.text))
        res2 = res2.json()["elements"]
        data = []

        for i in range(0, len(ids)):
            d = {}
            d["text"] = res[i]["text"]["text"]
            d["id"] = ids[i]
            d.update(res2[i])
            
            res3 = requests.get("https://api.linkedin.com/v2/socialActions/urn:li:share:{}/comments".format(ids[i]), headers=headers)
            if res3.status_code != 200:
                raise Exception("Exception {} {}".format(res3.status_code, res3.text))
            coms =  res3.json()["elements"]
            if (len(coms)>0):
                comments = [{"text":x["message"]["text"], "id":x["id"]} for x in coms]
            else:
                comments = []
            # for j in comments:
            #     coms.append(j["message"]["text"])
            d["comments"] = comments
            d["activityURN"] = res[i]["activity"]

            data.append(d)

        if len(data) == 0:
            config_file.close()
            with open(Path.joinpath(date_dir,"linkedin/linkedin_posts.json"),"w") as ftp:
                json.dump({},ftp,indent=4,sort_keys=True)
            return
        fields["lastRetreived"] = data[0]["id"]
        config_file.seek(0)
        json.dump(fields,config_file)
        config_file.truncate()
        config_file.close()
        with open(Path.joinpath(date_dir,"linkedin/linkedin_posts.json"),"w") as ftp:
            json.dump(data,ftp,indent=4,sort_keys=True)
    except Exception as e:
        logging.error('linkedin.posts_stats ', exc_info=True)

def get_metadata():
    #   ONE TIME USE ONLY, FOR REFERENCE PURPOSE
    data = {}
    headers = {
        'Authorization': 'Bearer {}'.format(config("linkedin_access_token"))
    }
    res = requests.get("https://api.linkedin.com/v2/regions",headers=headers)
    data = res.json()["elements"]
    with open(Path.joinpath(parent_dir,"metadata/linkedin_regions.json"),"w") as ftt:
        json.dump(data,ftt)


    res = requests.get("https://api.linkedin.com/v2/seniorities",headers=headers)
    data = res.json()["elements"]
    with open(Path.joinpath(parent_dir,"metadata/linkedin_seniority.json"),"w") as ftt:
        json.dump(data,ftt)
    res = requests.get("https://api.linkedin.com/v2/functions",headers=headers)
    data = res.json()["elements"]
    with open(Path.joinpath(parent_dir,"metadata/linkedin_function.json"),"w") as ftt:
        json.dump(data,ftt)
    res = requests.get("https://api.linkedin.com/v2/industries",headers=headers)
    data = res.json()["elements"]
    with open(Path.joinpath(parent_dir,"metadata/linkedin_industry.json"),"w") as ftt:
        json.dump(data,ftt)
    res = requests.get("https://api.linkedin.com/v2/countries",headers=headers)
    data = res.json()["elements"]
    with open(Path.joinpath(parent_dir,"metadata/linkedin_country.json"),"w") as ftt:
        json.dump(data,ftt)

def main(date_dir):
    path = Path.joinpath(parent_dir, "logs/logs.log")
    logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    try:
        # get_stats()
        posts_stats(date_dir)
        get_stats(date_dir)
    except Exception:
        logging.error("Exception",exc_info=True)

# if __name__ == "__main__":
    # 