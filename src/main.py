import os
import sys
import json
import logging
from pathlib import Path
import datetime
import reddit_data, reddit_posts_comments, instagram, linkedin, facebook, linux_packages, mastodon,twitter_data,youtube
import utils.linkedin_auth as lauth

parent_dir = Path(__file__).parent.parent.absolute()

def handler(date_dir):
    try:
        # reddit
        reddit_data.main(date_dir)
        logging.info("reddit_data file success")
        reddit_posts_comments.main(date_dir)
        logging.info("reddit_posts_comments file success")
        # instagram
        instagram.main(date_dir)
        logging.info("instagram file success")
        #facebook
        facebook.main(date_dir)
        logging.info("facebook file success")
        #linkedin
        #check auth
        if (lauth.main()):
        #get_data
            linkedin.main(date_dir)
            logging.info("linkedin file success")
        else:
            logging.error("linkedin access Token Error")
        #youtube
        youtube.main(date_dir)
        logging.info("youtube file success")
        #mastodon
        mastodon.main(date_dir)
        logging.info("mastodon file success")
        #linux
        linux_packages.main(date_dir)
        logging.info("packages file success")
        #twitter
        twitter_data.main(date_dir)
        logging.info("twitter file success")
    except Exception as e:
        logging.error("main.handler returned error", exc_info=True)
        
def create_directory():
    #weekly function
    try:
        data_dir_path = Path.joinpath(parent_dir, "data")
        if (not os.path.isdir(data_dir_path)):
            os.mkdir(data_dir_path)
        
        #date subdirectory

        date = datetime.datetime.now().date()
        dir_name = str(date.day) + "-" +str(date.month)+ "-" + str(date.year)
        date_dir = Path.joinpath(data_dir_path, dir_name )
        
        #social subdirectories

        social = [ "youtube", "twitter", "facebook", "mastodon", "instagram", "linkedin", "popcons", "reddit"]
        social = [Path.joinpath(date_dir,i) for i in social]

        for i in social:
            os.makedirs(i)
        return date_dir
    except FileExistsError:
        return date_dir
    except Exception as e:
        logging.error("main.create_directory", exc_info=True)

if __name__ == "__main__":
    try:
        path = Path.joinpath(parent_dir, "logs/logs.log")
        logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    except FileNotFoundError:
        logging.basicConfig(filename=path, filemode="w",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    try:
        date_dir = create_directory()
        handler(date_dir)
    except Exception as e:
        logging.error("main.handler exception", exc_info=True)
