import requests
from decouple import config
import datetime
from pathlib import Path
import logging 
import json

parent_dir = Path(__file__).parent.parent.absolute()

def get_instagram(date_dir):
    url1 = "https://graph.facebook.com/v9.0/{}/insights?metric=website_clicks,profile_views&period=day&access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
    url2 = "https://graph.facebook.com/v9.0/{}/insights?metric=audience_city,audience_country,audience_gender_age&period=lifetime&access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
    url3 = "https://graph.facebook.com/v9.0/{}/insights?metric=reach,impressions&period=week&access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
    url4 = "https://graph.facebook.com/v9.0/{}?fields=followers_count%2Cmedia_count&access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
    try:

        resp = requests.get(url4)

        if (resp.status_code != 200):
            raise Exception("Instagram returned error {} {}".format(resp.status_code,resp.text))

        follower_count = resp.json()

        resp = requests.get(url1)
        
        if (resp.status_code != 200):
            raise Exception("Instagram returned error {} {}".format(resp.status_code,resp.text))

        f = resp.json()["data"]

        resp = requests.get(url2)

        if (resp.status_code != 200):
            raise Exception("Instagram returned error {} {}".format(resp.status_code,resp.text))

        f+=resp.json()["data"]

        resp = requests.get(url3)

        if (resp.status_code != 200):
            raise Exception("Instagram returned error {} {}".format(resp.status_code,resp.text))

        f+=resp.json()["data"]

        data = {}
        data.update(follower_count)
        for i in f:
            if (len(i["values"]) > 1):
                sd = i["values"]
                count = 0
                for j in sd:
                    count+=j["value"]

                data[i["title"]] = count
            else:
                data[i["title"]] = i["values"][0]["value"]
        
        with open(Path.joinpath(date_dir, "instagram/instagram_stats.json"),"w") as fpt:
            json.dump(data,fpt)
        logging.info("Created Instagram File")
    except Exception as e:
        logging.error("Exception",exc_info=True)

def instagram_post_metrics(date_dir):
    #instagram user api does not support filters,so since we are not posting many posts each week i am going through the results are storing
    #the posts `since` the last stored id.

    try:
        f = open(Path.joinpath(parent_dir, "metadata/insta-config.json"), "r+")
        f_read = json.loads(f.read())
        last = f_read["since_id"]

        url = "https://graph.facebook.com/v9.0/{}/media?access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
        
        resp = requests.get(url)
        
        if(resp.status_code != 200):
            raise Exception("Instagram returned error {} {}".format(resp.status_code, resp.text))

        post = resp.json()["data"]
        post_data = []
        
        while (not last in post):
            next_url = resp.json()["paging"]["next"]
            res = requests.get(next_url)
            if (res.status_code == 200):
                post += res.json()["data"]
            else:
                logging.error("Instagram Next URL returned error {}".format(res.status_code))

        index = post.index(last)
        # print(post)
        # print(index)
        post = post[0:index]

        if len(post) == 0:
            with open(Path.joinpath(date_dir, "instagram/instagram_posts_stats.json"),"w") as fptr:
                json.dump({},fptr)
            return

        last_index_new = post[0]
        f_read["since_id"] = last_index_new
        f.seek(0)
        json.dump(f_read,f)
        f.truncate()
        f.close()
        for i in post:
            url1 = "https://graph.facebook.com/v9.0/{}/insights?metric=engagement%2Cimpressions%2Creach%2Csaved&access_token={}".format(i["id"], config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
            url2 = "https://graph.facebook.com/v9.0/{}/?fields=comments_count%2Ccomments%2Clike_count%2Cpermalink%2Ccaption&access_token={}".format(i["id"], config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
            data = {}
            try:
                res = requests.get(url1)
                if (res.status_code != 200):
                    raise Exception("Media Error {} {}".format(res.status_code,res.text))
                    res = res.json()
                    rd1 = res["data"]
                    data = {}
            
                    for i in rd1:
                        if (len(i["values"]) > 1):
                            sd = i["values"]
                            count = 0
                            for j in sd:
                                count+=j["value"]
                            data[i["name"]] = count
                        else:
                            data[i["name"]] = i["values"][0]["value"]

                res = requests.get(url2)
                if (res.status_code != 200):
                    logging.error("Error {}".format(res.text))
                    break
                rd2 = res.json()

                data["comments_count"] = rd2["comments_count"]
                data["like_count"] = rd2["like_count"]
                data["caption"] = rd2["caption"]
                data["permalink"] = rd2["permalink"]
                data["id"] = i["id"]
                try:
                    if (data["comments_count"] > 0):
                        com = rd2["comments"]["data"]
                        data["comments"] = com
                except KeyError:
                    data["comments"] = rd2
            except Exception as e:
                logging.error("Exception for {}".format(i["id"]), exc_info=True)

            post_data.append(data)
        # print(post_data)
        with open(Path.joinpath(date_dir, "instagram/instagram_posts_stats.json"),"w") as fptr:
            json.dump(post_data,fptr)
        logging.info("Created Instagram posts File")
    except Exception as e:
        logging.error("Exception ", exc_info=True)

def get_mentions():
    #https://developers.facebook.com/docs/instagram-api/guides/mentions/
    #TODO
    #currently permissions not given
    pass

def main(date_dir):
    path = Path.joinpath(parent_dir, "logs/logs.log")
    logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    try:
        get_instagram(date_dir)
        instagram_post_metrics(date_dir)
    except Exception as e:
        logging.debug("Exception",exc_info=True)


# if __name__ == "__main__":
#     path = Path.joinpath(parent_dir, "logs/logs.log")
#     logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
#     try:
#         get_instagram()
#         instagram_post_metrics()
#     except Exception as e:
#         logging.debug("Exception",exc_info=True)