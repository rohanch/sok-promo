import praw
import requests
from pathlib import Path
import logging
import json
import time
from decouple import config
import  utils.sentiment as sentiment

parent_dir = Path(__file__).parent.parent.absolute()
reddit = praw.Reddit(client_id=config("REDDIT_CLIENT_ID"), client_secret=config("REDDIT_CLIENT_SECRET"), password=config(
    "REDDIT_PASSWORD"), username=config("REDDIT_USERNAME"), user_agent=config("REDDIT_USER_AGENT"))


def get_top_posts(date_dir):
    # get top posts of the week, can change in config file
    headers = {
        'User-Agent': 'promobot'
    }
    try:
        f = open(Path.joinpath(parent_dir, "metadata/reddit_config.json"), "r")
        fields = json.loads(f.read())
        f.close()
        names = fields["posts_subreddits"]
        time_f = fields["posts_time_filter"]
        post_f = fields["post_fields"]
        for i in names:
            url = "https://www.reddit.com/r/{}/top.json?t={}".format(i, time_f)
            resp = requests.get(url, headers=headers)
            if (resp.status_code != 200):
                raise Exception("Request returned an error: {} {}".format(
                    resp.status_code, resp.text))
            else:
                resp = resp.json()
                resp = resp["data"]["children"]
                data = []
                for j in resp:
                    d = {}
                    j = j["data"]
                    for k in post_f:
                        try:
                            d[k] = str(j[k])
                        except KeyError:
                            logging.debug("Key Not Found", exc_info=True)
                    # comments
                    try:
                        coms = reddit.submission(id=d["id"])
                        coms = coms.comments
                        comments = []
                        for l in coms:
                            c = {}
                            c["text"] = l.body
                            c["votes"] = l.ups
                            c["sentiment"] = sentiment.sentiment_analysis(c["text"])
                            comments.append(c)
                        d["comments"] = comments
                    except Exception as e:
                        logging.error(
                            "Getting Comments from posts failed", exc_info=True)

                    data.append(d)
                with open(Path.joinpath(date_dir, "reddit/{}_posts.json".format(i)), "w") as fptr:
                    json.dump(data, fptr)
    except Exception as e:
        logging.error("Exception", exc_info=True)


def get_all_posts_by_contributors(date_dir):
    time_now = time.time()
    # one week before
    time_start = time_now - 604800
    try:
        f = open(Path.joinpath(parent_dir, "metadata/reddit_config.json"), "r")
        fields = json.loads(f.read())
        f.close()
        usernames = fields["usernames"]
        data = {}
        for i in usernames:
            d = []
            posts = reddit.redditor(i).submissions.new()
            for j in posts:
                if (j.subreddit == "kde" and j.created > time_start):
                    c = {}
                    c["id"] = j.id
                    c["title"] = j.title
                    c["votes"] = j.ups
                    coms = []
                    comments = j.comments
                    for k in comments:
                        f = {}
                        f["text"] = k.body
                        f["votes"] = k.ups
                        f["sentiment"] = sentiment.sentiment_analysis(f["text"])
                        coms.append(f)
                    c["comments"] = coms
                    d.append(c)
            data[i] = d

        with open(Path.joinpath(date_dir, "reddit/contributor_posts.json"), "w") as fpt:
            json.dump(data, fpt)
    except Exception as e:
        logging.error("Exception", exc_info=True)

def main(date_dir):
    path = Path.joinpath(parent_dir, "logs/logs.log")
    logging.basicConfig(filename=path, filemode="a",
                        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    get_all_posts_by_contributors(date_dir)
    get_top_posts(date_dir)

# if __name__ == "__main__":
#     path = Path.joinpath(parent_dir, "logs/logs.log")
#     logging.basicConfig(filename=path, filemode="a",
#                         format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
#     get_all_posts_by_contributors()
