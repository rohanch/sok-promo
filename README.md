# Social Media Data Collection Automation

As part of Season Of Kde, this project was made to automate retreiving data from different social media platforms.

Social media platforms supported:

- Facebook
- Instagram
- Reddit
- Youtube
- Mastodon
- Twitter
- Linkedin

## What data is being collected?

## Facebook

Facebook data about a page or an account is private and can only be accessed after logging-in with facebook to a third party app. I used developer tools to get a lifetime page-access-token and user-access-token, for accessing data from the facebook Graph API.

The following data is retreived every week:

* Page Stats
    * Engagement
    * Impressions
    * Followers
    * Stats about followers like country, language, region, gender, age
    * Change in followers
* Page post stats
    * Post impressions, engagement, likes, number of comments, comments.
    * For each post the data will look like this

```json
{
        "post_engaged_users": 222,
        "created_time": "2021-04-04T09:56:14+0000",
        "message": "This Week in KDE" 
        "id": "6344818917_10158443962153918",
        "post_negative_feedback": 1,
        "post_negative_feedback_unique": 1,
        "post_negative_feedback_by_type": {
            "hide_all_clicks": 1
        },
        "post_clicks_unique": xxx,
        "post_clicks": xxx,
        "post_engaged_fan": xxx,
        "post_impressions": xxx,
        "post_impressions_unique": xxx,
        "post_impressions_fan": xxx,
        "post_impressions_organic": xxx,
        "comments": [
            {
                "created_time": "2021-04-06T13:32:08+0000",
                "message": "xxx. ",
                "id": "xxx"
            },
        ]
}
```

## Instagram

Instagram uses same authentication and api same as facebook. 

The following data is retreived every week:

* Page stats
    * Followers count
    * Website clicks 
    * Profile Views
    * Followers demographics (Location, age, gender, country)
    * Reach, Impressions
* Post stats: for each post posted that week.
    * like count, comment count, comment list

## Linkedin

Using the linkedin marketing api, the following data is being collected.

* Follower statistics
    * followers by region, seniority, industry, function (engineering, it, sales, etc), staff_count, country
* Page statistics
    * page statitics by region, seniority, industry, function (engineering, it, sales, etc), staff_count, country
    
    For each of the above sub-categories, we measure different page views like mobilePageViews, desktopPageViews, careersPageViews, etc.

    `For example. We got 3 careerPageViews from the group seniority:unpaid.`

    These fields are collected for every category of every subcategory.

```json
    "mobileProductsPageViews": {
        "pageViews": 0
    },
    "allDesktopPageViews": {
        "pageViews": 4
    },
    "insightsPageViews": {
        "pageViews": 0
    },
    "jobsPageViews": {
        "pageViews": 0
    },
    "productsPageViews": {
        "pageViews": 0
    },
    "desktopProductsPageViews": {
        "pageViews": 0
    },
    "peoplePageViews": {
        "pageViews": 0
    },
    "lifeAtPageViews": {
        "pageViews": 0
    },
    "desktopOverviewPageViews": {
        "pageViews": 4
    },
    "mobileCareersPageViews": {
        "pageViews": 0
    },
    "allPageViews": {
        "pageViews": 37
    },
    "mobileJobsPageViews": {
        "pageViews": 0
    },
    ......
    }
```

* Post stats
    * comments list, clickcount, engagement, impressions, likes, shares, 

* Linkedin organisation stats
    * These are lifetime count: followerCount, uniqueImpressionCount,likecount, commentCount, ...

* Weekly follower stats
    * In a week how many followers we have gained day-by-day stats.

* Weekly page views
    * Total page views of the week.

**NOTE: For Linkedin, Facebook, Instagram we cannot swap the account id getting for metrics of a different account, because we need to authenticate for that account also. We can integrate authentication for getting private metrics of any account, but since we are running this as a script on server it is not possible without a GUI.**

## Mastodon

* Account public infomation: number of followers, number of toots
* Toots public information: likes, reblogs count, replies count.

## Linux Popcons/ Usage statistics

* From popular linux distros like ubuntu, arch, debain we are retreiving usage statistics of KDE packages. We get the popularity rank of a given package among all the packages. This can be used to partially determine which packages/applications are more popular.

## Twitter

Public data like number of followers, number of tweets of any account.
Public data like number of likes, retweets of a user in the past week.

Private data like impressions, reach for @kdecommunity twitter account.

## Youtube

Using the youtube public data api, so that we dont have to deal with authentication using kde's google account. The following data is retreived:

* Channel stats
    * Channel stats like subsribers count.
* Content stats
    * Statistics for videos posted on a channel like viewCount, likes, dislikes, commentCount, comment list, etc.

We can add a new channel id in the `metadata/youtube-config` for getting stats about more channels of the form `[channelId, null]`

## Reddit

* Contributor posts
    * Retreive all posts made by KDE contributors on behalf of the r/kde subreddit. 
    * We can add or remove usernames from the list in `metadata/reddit_config.json`.
* Subreddit stats
    * Gets statistics about a subreddit like subscribers count, number of posts, number of awards, etc.
    * We can add or remove subreddits in the `subreddit_names` field of config file.
* Keyword posts
    * Searches reddit archive for all posts with certain keywords. I thought of doing text analysis on posts, data was not enough so I am doing this after some time.


## Cloning and running

```
git clone https://invent.kde.org/rohanch/sok-promo.git
cd sok-promo
virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt
python3 setup.py
python3 src/main.py
```

Make sure you have the .env file in the project root.
