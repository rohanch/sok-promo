from collections import Counter
import requests
from decouple import config
import time
from pathlib import Path
import logging 
import json

#TODO MENTIONS; CURRENTLY PERMISSIONS NOT GIVEN

parent_dir = Path(__file__).parent.parent.absolute()

def facebook_stats(date_dir):
    try:
        end_time = int(time.time())
        start_time = end_time - 604810

        url1 ="https://graph.facebook.com/v9.0/6344818917/insights?&since={}&until={}&metric=page_engaged_users,page_post_engagements,page_impressions,page_impressions_organic,page_impressions_paid,page_consumptions,page_negative_feedback,page_video_views,&period=day&access_token={}".format(start_time,end_time,config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
        url2 ="https://graph.facebook.com/v9.0/6344818917/insights?&since={}&until={}&metric=page_negative_feedback_by_type,page_positive_feedback_by_type&period=day&access_token={}".format(start_time,end_time,config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
        url3 ="https://graph.facebook.com/v9.0/6344818917/insights?&since={}&until={}&metric=page_fans,page_fans_locale,page_fans_city,page_fans_gender_age,page_fan_adds_unique,page_fan_removes_unique&period=day&access_token={}".format(start_time,end_time,config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))

        data = {}
        res = requests.get(url1)
        res = res.json()["data"]

        for i in res:
            data[i["name"]] = sum(x["value"] for x in i["values"])

        res = requests.get(url2)
        res = res.json()["data"]

        for i in res:
            vals = i["values"]
            cu = Counter({})
            for j in vals:
                cu = cu + Counter(j["value"])
            data[i["name"]] = dict(cu)
        
        res = requests.get(url3)
        res = res.json()["data"]

        for i in res:
            data[i["name"]] = i["values"][-1]["value"]
        
        with open(Path.joinpath(date_dir, "facebook/facebook_stats.json"),"w") as fpt:
            json.dump(data,fpt)
        logging.info("Facebook stats file created")
    except Exception as e:
        logging.error("Exception",exc_info=True)

def facebook_posts(date_dir):
    try:
        f = open(Path.joinpath(parent_dir, "metadata/fb-config.json"), "r+")
        f_read = json.loads(f.read())
        last = f_read["since_id"]
        url =  "https://graph.facebook.com/v9.0/6344818917/posts?access_token={}".format(config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))

        
        resp = requests.get(url)
        if (resp.status_code != 200):
            raise Exception("Exception {} {}".format(resp.status_code,resp.text))
        post = resp.json()["data"]

        while (not last in post):
            next_url = resp.json()["paging"]["next"]
            res = requests.get(next_url)
            if (res.status_code == 200):
                post += res.json()["data"]
            else:
                logging.error("Instagram Next URL returned error {}".format(res.status_code))
        
        index = post.index(last)
        post = post[0:index]

        if len(post) == 0:
            with open(Path.joinpath(date_dir, "facebook/facebook_posts_stats.json"),"w") as fptr:
                json.dump({},fptr)
            return

        last_index_new = post[0]
        f_read["since_id"] = last_index_new
        f.seek(0)
        json.dump(f_read,f)
        f.truncate()
        f.close()
        
        post_data = []

        for i in post:
            url =  "https://graph.facebook.com/v9.0/{}/insights?metric=post_engaged_users%2Cpost_negative_feedback%2Cpost_negative_feedback_unique%2Cpost_negative_feedback_by_type%2Cpost_clicks_unique%2Cpost_clicks%2Cpost_engaged_fan%2Cpost_impressions%2Cpost_impressions_unique%2Cpost_impressions_fan%2Cpost_impressions_organic&access_token={}".format(i["id"],config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
            coms = "https://graph.facebook.com/v9.0/{}/comments?access_token={}".format(i["id"],config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
            data = {}
            res = requests.get(url)
            res = res.json()
            for j in res["data"]:
                data[j["name"]] = j["values"][0]["value"]
                data.update(i)
            
            res = requests.get(coms)
            data["comments"] = res.json()["data"]

            post_data.append(data)

        with open(Path.joinpath(date_dir, "facebook/facebook_posts_stats.json"),"w") as fptr:
            json.dump(post_data,fptr)
        logging.info("Facebook posts file created")
    except Exception as E:
        logging.error("Exception",exc_info=True)

def main(date_dir):
    path = Path.joinpath(parent_dir, "logs/logs.log")
    logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    try:
        facebook_stats(date_dir)
        facebook_posts(date_dir)
    except Exception as e:
        logging.error("Exception",exc_info=True)

# if __name__ == "__main__":
#     path = Path.joinpath(parent_dir, "logs/logs.log")
#     logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
#     try:
#         facebook_stats()
#         facebook_posts()
#     except Exception as e:
#         logging.error("Exception",exc_info=True)