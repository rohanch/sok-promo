import re 
import requests
import os
# from bs4 import BeautifulSoup
import pickle
import datetime
import json
import gzip
from pathlib import Path
import logging

parent_dir = Path(__file__).parent.parent.absolute()

#TODO if any other distro then that.

def get_names():
    #one time use, scrape kde website to get package names
    URL = 'https://apps.kde.org/'

    resp = requests.get(URL)

    soup = BeautifulSoup(resp.content, 'html.parser')

    div = soup.findAll("div", class_="p-3 h-100")

    names = []

    for i in div:
        t = i.findAll('a')[1].text
        t = t.strip()
        names.append(t)
    return names

def read_file():
    #one time use, get names of packages and store in pickle file
    if (not os.path.isfile("metadata/archpackages.txt")):
        f = open("metadata/archpackages.txt","w")
        names = get_names()

        final = []

        for i in names:
            url = "https://pkgstats.archlinux.de/api/packages/{}?startMonth=201901&endMonth=202001".format(i)
            resp = requests.get(url)
            if (resp.status_code == 200):
                print(resp.json())
                final.append(i)
            else:
                print(resp.status_code, i)
        
        for item in final:
            f.write("%s\n" % item)
        f.close()
    
    if (not os.path.isfile("metadata/packages.pyc")):
        names = []

        f = open("metadata/archpackages.txt","r")
        cont = f.read()
        cont = list(cont.split("\n"))

        with open('metadata/packages.pyc', 'wb') as fp:
            pickle.dump(cont, fp)

def get_arch(date_dir):
    try:
        #arch linux packages stats
        packages = ["kirigami2","kirigami-gallery","KArchive","ThreadWeaver","breeze-icons","KI18n"]
        packages_query = ["plasma","kconfig","kwin"]
        

        # with open ('metadata/packages.pyc', 'rb') as fp:
        #     itemlist = pickle.load(fp)
        #     fp.close()
        with open("metadata/archpackages.txt",'r') as fp:
            itemlist = fp.read().split("\n")
        responses = []
        #gives results month by month 
        startmonth = datetime.datetime.now().strftime("%G") + datetime.datetime.now().strftime("%m")
        print("Making package requests ... ")
        for i in itemlist:
            url = "https://pkgstats.archlinux.de/api/packages/{}?startMonth={}&endMonth={}".format(i,startmonth,startmonth)
            res = requests.get(url)
            try:
                if(res.status_code != 200):
                    print("Error {} {}",res.status_code, res.text)
                    responses.append(res.json())
            except Exception as e:
                logging.error("Returned error", exc_info=True)

        print("Making package requests ... ")
        for i in packages:
            url = "https://pkgstats.archlinux.de/api/packages/{}?startMonth={}&endMonth={}".format(i,startmonth,startmonth)
            res = requests.get(url)
            if(res.status_code != 200):
                print("Error {} {}",res.status_code, res.text)
                break
            responses.append(res.json())
        print("Making batch requests ... ")
        for i in packages_query:
            url = "https://pkgstats.archlinux.de/api/packages?limit=150&offset=0&query={}".format(i)
            res = requests.get(url)
            if(res.status_code != 200):
                print("Error {} {}",res.status_code, res.text)
                break
            res = res.json()
            res = res["packagePopularities"]
            responses += res
        
        with open(Path.joinpath(date_dir, "popcons/arch.json"), "w") as ft:
            json.dump(responses,ft)

        ft.close()
    except Exception as e:
        logging.error("Exception linux_packages.arch", exc_info=True)

def clean_response_ubuntu(res):
    content = res[446:]
    content = re.sub(" +"," ",content)
    content = content.split("\n")
    con = []
    for i in content:
        con.append(i.split())
    del content
    con.pop(0)
    for i in range(3):
        con.pop(-1)
    return con

def ubuntu_package_names():
    #utility func
    f = open(Path.joinpath(parent_dir, "metadata/archpackages.txt"), "r")
    c = list(f.read().split("\n"))
    f = open(Path.joinpath(parent_dir, "metadata/ubuntupackages.txt"), "w")
    for i in c:
        f.write(i.lower()+"\n")
    f.close()

def get_ubuntu(date_dir):
    res = requests.get("https://popcon.ubuntu.com/main/by_vote")
    res = res.text
    data = clean_response_ubuntu(res)

    res = requests.get("https://popcon.ubuntu.com/universe/by_inst")
    data+= clean_response_ubuntu(res.text) 

    f = open(Path.joinpath(parent_dir, "metadata/ubuntupackages.txt"), "r")
    c = list(f.read().split("\n"))
    # print(c)
    out = []
    fields = ['#rank', 'name', 'inst', 'vote', 'old', 'recent', 'no-files', '(maintainer)']
    
    for i in data:
        d = {}
        if i[1] in c:
            d[fields[1]] = i[1]
            d[fields[2]] = i[2]
            d[fields[3]] = i[3]
            d[fields[4]] = i[4]
            d[fields[5]] = i[5]
            out.append(d)
    with open(Path.joinpath(date_dir, "popcons/ubuntu.json"), "w") as ft:
        json.dump(out, ft)

def get_debian_packages(date_dir):
    resp = requests.get("https://popcon.debian.org/all-popcon-results.gz")
    raw = gzip.decompress(resp.content)
    raw = raw.decode("utf-8")
    clean = re.sub(" +"," ",raw)
    del raw
    clean = clean.split("\n")
    data = [i.split() for i in clean]
    data.pop(-1)
    del clean
    out = []
    fields = ['#package', 'name', 'inst', 'vote', 'old', 'recent', 'no-files', '(maintainer)']
    f = open(Path.joinpath(parent_dir, "metadata/ubuntupackages.txt"), "r")
    names = f.read().split("\n")
    for i in names:
        d = {}
        for j in data:
            if j[1] == i:
                d[fields[1]] = j[1]
                d[fields[2]] = j[2]
                d[fields[3]] = j[3]
                d[fields[4]] = j[4]
                d[fields[5]] = j[5]
                out.append(d)
                break
    with open(Path.joinpath(date_dir, "popcons/debian.json"), "w") as ft:
        json.dump(out, ft)

def main(date_dir):
    path = Path.joinpath(parent_dir, "logs/logs.log")
    logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    try:
        get_ubuntu(date_dir)
        get_arch(date_dir)
        get_debian_packages(date_dir)
    except Exception as e:
        logging.error("linux_packages", exc_info=True)    
# if __name__ == "__main__":
#     # read_file()
#     get_arch()
#     get_ubuntu()
#     # ubuntu_package_names()
#     get_debian_packages()
