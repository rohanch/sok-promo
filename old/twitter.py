import requests
import os
import json
from decouple import config

#1. get all tweets with any hashtags related to kde
#2. get all tweets with any keywords related to kde
#3. get all replies to @kdecommunity's tweets
# hastags,keywwords and other config can be added or removed in metadata/twitter_config.json


def auth():
    return config("TWITTER_BEARER_TOKEN")

def create_headers(bearer_token):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    return headers

def create_url():
    query = "to:kdecommunity"
    return "https://api.twitter.com/2/tweets/search/recent?query={}".format(query)


def connect_to_endpoint(url, headers):
    response = requests.request("GET", url, headers=headers, stream=True)
    print(response.status_code)
    for response_line in response.iter_lines():
        if response_line:
            json_response = json.loads(response_line)
            print(json.dumps(json_response, indent=4, sort_keys=True))
    if response.status_code != 200:
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )


def main():
    bearer_token = auth()
    url = create_url()
    headers = create_headers(bearer_token)
    timeout = 0
    connect_to_endpoint(url,headers)


if __name__ == "__main__":
    main()