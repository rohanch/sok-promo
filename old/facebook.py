import requests
from decouple import config
import datetime
import json

#TODO add try except and resp.statuscode error handling
def get_instagram():
    url = "https://graph.facebook.com/v9.0/{}/insights?metric=website_clicks,profile_views&period=day&access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
    url2 = "https://graph.facebook.com/v9.0/{}/insights?metric=audience_city,audience_country,audience_gender_age&period=lifetime&access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
    url3 = "https://graph.facebook.com/v9.0/{}/insights?metric=reach,impressions&period=week&access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))

    resp = requests.get(url)

    
    if (resp.status_code == 200):
        f = resp.json()["data"]

    resp = requests.get(url2)

    if (resp.status_code == 200):
        f+=resp.json()["data"]

    resp = requests.get(url3)

    if (resp.status_code == 200):
        f+=resp.json()["data"]
    
    #change this after format 
    fp = open("ig_user_insights{}.json".format(datetime.datetime.now()), "w")
    a = {}
    for i in f:
        if (len(i["values"]) > 1):
            sd = i["values"]
            count = 0
            for j in sd:
                count+=j["value"]

            a[i["title"]] = count
        else:
            a[i["title"]] = i["values"][0]["value"]

    json.dump(a,fp)    

def check_array(pos,lastid):
    for i in pos:
        if i["id"] == lastid:
            return True
    else:
        return False

def instagram_post_metrics():
    #instagram user api does not support filters,so since we are not posting many posts each week i am going through the results are storing
    #the posts `since` the last stored id.
    lastid = config("LAST_RETREIVED_INSTAGRAM_POST")
    url = "https://graph.facebook.com/v9.0/{}/media?access_token={}".format(config("INSTAGRAM_KDE_PAGE_ID"), config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
    
    resp = requests.get(url)
    if(resp.status_code != 200):
        #TODO raise error
        return
    pos = resp.json()["data"]
    post_data = []
    posts = []
    #TODO next token also 
    # while (not check_array(pos,lastid)):



    for i in pos:
        url2 = "https://graph.facebook.com/v9.0/{}/?fields=comments_count%2Ccomments%2Clike_count%2Cpermalink%2Ccaption&access_token={}".format(i["id"], config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
        url = "https://graph.facebook.com/v9.0/{}/insights?metric=engagement%2Cimpressions%2Creach%2Csaved&access_token={}".format(i["id"], config("FACEBOOK_KDE_PAGE_ACCESS_TOKEN"))
        res = requests.get(url)
        if (res.status_code != 200):
            print("Error {}".format(res.text))
            break
        
        res = res.json()
        rd1 = res["data"]

        data = {}
        
        for i in rd1:
            if (len(i["values"]) > 1):
                sd = i["values"]
                count = 0
                for j in sd:
                    count+=j["value"]

                data[i["name"]] = count
            else:
                data[i["name"]] = i["values"][0]["value"]

        res = requests.get(url2)
        if (res.status_code != 200):
            print("Error {}".format(res.text))
            break
        rd2 = res.json()

        data["comments_count"] = rd2["comments_count"]
        data["like_count"] = rd2["like_count"]
        data["caption"] = rd2["caption"]
        data["permalink"] = rd2["permalink"]

        

        if (data["comments_count"] > 0):
            com = rd2["comments"]["data"]
            data["comments"] = com

        post_data.append(data)
    pd = {}
    pd["posts_data"] = post_data
    with open("post_{}.json".format(datetime.datetime.now()), "w") as fptr:
        json.dump(pd,fptr)

if __name__ == "__main__":
    # get_instagram()
    instagram_post_metrics()
    