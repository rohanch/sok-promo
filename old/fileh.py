import os
import datetime
import app
# import dask.dataframe as dd
import pandas as pd
import gc
import sys

def check_files():
    if (not os.path.isdir("data")):
        try:
            os.mkdir("data")
            print("data directory created")
        except OSError:
            sys.exit("Cannot create Directory")
    if (not os.path.isfile("data/twitter_overall.csv")):
        f = open("data/twitter_overall.csv", "w")
        f.write(
            "date,num_followers,increment_in_followers,num_posts,increment_in_posts,users_per_tweets\n")
        f.write("{},0,0,0,0,0\n".format(
            datetime.datetime.now().strftime("%x")))
        f.close()
        print("twitter data file created")
    if (not os.path.isfile("data/mastodon_overall.csv")):
        f = open("data/mastodon_overall.csv", "w")
        f.write(
            "date,num_followers,increment_in_followers,num_toots,increment_in_toots,users_per_toots\n")
        f.write("{},0,0,0,0,0\n".format(
            datetime.datetime.now().strftime("%x")))
        f.close()
        print("mastodon data file created")
    if (not os.path.isfile("data/reddit_subs.csv")):
        f = open("data/reddit_subs.csv", "w")
        f.write(
            "date,num_subs,increment_in_subs\n")
        f.write("{},0,0\n".format(
            datetime.datetime.now().strftime("%x")))
        f.close()
        print("reddit data file created")
    if (not os.path.isfile("data/linkedin_stats.csv")):
        f = open("data/linkedin_stats.csv", "w")
        f.write("date,followers,increment_in_followers\n")
        f.write("{},0,0\n".format(
            datetime.datetime.now().strftime("%x")))
        f.close()

def update_twitter(data):
    try:
        df = pd.read_csv("data/twitter_overall.csv")
        old = df.tail(1)
    except FileNotFoundError:
        print("Twitter File Not Found")
        return
    data["increment_in_followers"] = data["num_followers"] - int(old["num_followers"])
    data["increment_in_posts"] = data["num_posts"] - int(old["num_posts"])
    data["date"] = datetime.datetime.now().strftime("%x")
    try:
        data["users_per_tweets"] = data["increment_in_followers"] / data["increment_in_posts"]
    except ZeroDivisionError:
        data["users_per_tweets"] = 0
    try:
        new_df = pd.DataFrame([data.values()], columns=data.keys())

        df = df.append(new_df, ignore_index=True, verify_integrity=True)

        df.to_csv("data/twitter_overall.csv", mode="w",
                  index=False, header=True, line_terminator="\n")
    except:
        print("Error Occurred while Saving dataframe as csv")

def update_mastodon(data):
    try:
        df = pd.read_csv("data/mastodon_overall.csv")
        old = df.tail(1)
    except FileNotFoundError:
        print("Twitter File Not Found")
        return
    try:
        data["increment_in_followers"] = data["num_followers"] - int(old["num_followers"])
        data["increment_in_toots"] = data["num_toots"] - int(old["num_toots"])
        data["date"] = datetime.datetime.now().strftime("%x")
        try:
            data["users_per_toots"] = data["increment_in_followers"] / data["increment_in_toots"]
        except ZeroDivisionError:
            data["users_per_toots"] = 0
        new_df = pd.DataFrame([data.values()], columns=data.keys())

        df = df.append(new_df, ignore_index=True, verify_integrity=True)

        df.to_csv("data/mastodon_overall.csv", mode="w",index=False, header=True, line_terminator="\n")
    except Exception as e:
        print("Error Occured while storing data in mastodon csv.")

def update_reddit(data):
    try:
        df = pd.read_csv("data/reddit_subs.csv")
        old = df.tail(1)
    except FileNotFoundError:
        print("Reddit File Not Found")
        return
    try:
        data["increment_in_subs"] = data["num_subs"] - int(old["num_subs"])
        data["date"] = datetime.datetime.now().strftime("%x")
        new_df = pd.DataFrame([data.values()], columns=data.keys())
        df = df.append(new_df, ignore_index=True, verify_integrity=True)
        df.to_csv("data/reddit_subs.csv", mode="w",index=False, header=True, line_terminator="\n")
    except Exception as e:
        print("Error Occured while storing data in reddit csv.")

def update_linkedin(data):
    try:
        df = pd.read_csv("data/linkedin_stats.csv")
        old = df.tail(1)
    except FileNotFoundError:
        print("Linkedin File Not Found")
        return
    try:
        data["increment_in_followers"] = data["followers"] - int(old["followers"])
        data["date"] = datetime.datetime.now().strftime("%x")
        new_df = pd.DataFrame([data.values()], columns=data.keys())
        df = df.append(new_df, ignore_index=True, verify_integrity=True)
        df.to_csv("data/linkedin_stats.csv", mode="w",index=False, header=True, line_terminator="\n")
    except Exception as e:
        print("Error Occured while storing data in linkedin csv.")

if __name__ == "__main__":
    print("Checking Files")
    check_files()
    print("Getting Twitter data")
    update_twitter(app.get_twitter_stats())
    gc.collect()
    print("Writing Twitter Data")
    print("Getting Mastodon data")
    update_mastodon(app.get_mastodon_stats())
    print("Getting and Writing Reddit Data")
    update_reddit(app.get_reddit())
    print("Updating Linkedin")
    update_linkedin(app.get_linkedin())
    gc.collect()

