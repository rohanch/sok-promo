import os
import json
import requests
from decouple import config
import praw


def get_twitter_stats():
    twitter_endpointURL = "https://api.twitter.com/2/users/by?usernames=kdecommunity&user.fields=public_metrics"
    twitter_headers = {
        "Authorization": "Bearer {}".format(config("TWITTER_BEARER_TOKEN"))}
    response = requests.request(
        "GET", twitter_endpointURL, headers=twitter_headers)
    if response.status_code != 200:
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )
    else:
        response = response.json()
        response = response["data"][0]["public_metrics"]
        result = {
            "num_followers": response["followers_count"], "num_posts": response["tweet_count"]}
        return result


def get_mastodon_stats():
    mURL = "https://mastodon.technology/api/v1/accounts/42572"
    response = requests.get(mURL)
    if (response.status_code != 200):
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )
    else:
        response = response.json()
        result = {
            "num_followers": response["followers_count"], "num_toots": response["statuses_count"]}
        return result


def get_reddit():
    # r/kde number of active members
    headers = {
        "User-Agent": "don't rate limit me"
    }
    url = "http://www.reddit.com/r/kde/about.json"
    response = requests.get(url, headers=headers)
    if (response.status_code != 200):
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )
    else:
        response = response.json()
        result = {"num_subs":response["data"]["subscribers"]}
        return result
    


def get_linkedin():
    # followers_url = "https://api.linkedin.com/v2/organizationalEntityFollowerStatistics?q=organizationalEntity&organizationalEntity=urn:li:organization:{}".format(config("LINKEDIN_MY_ORGID"))
    # page_stats_url = "https://api.linkedin.com/v2/organizationPageStatistics?q=organization&organization=urn:li:organization:{}"
    Organization_Follower_Count = "https://api.linkedin.com/v2/networkSizes/urn:li:organization:{}?edgeType=CompanyFollowedByMember".format(config("LINKEDIN_MY_ORGID"))
    headers = {
        'Authorization': 'Bearer {}'.format(config("linkedin_access_token"))
    }
    payload = {}
    response = requests.request(
        "GET", Organization_Follower_Count, headers=headers, data=payload)
    response = response.json()
    result = {}
    result["followers"] = response["firstDegreeSize"]
    return result

    # response2 = requests.request(
    #     "GET", page_stats_url, headers=headers, data=payload)
    # print(response2.text)
