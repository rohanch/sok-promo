import os
import json
import requests
import logging
from decouple import config
from pathlib import Path

parent_dir = Path(__file__).parent.parent.absolute()

def get_mastodon_stats(date_dir):
    try:
        mURL = "https://mastodon.technology/api/v1/accounts/{}".format(42572)
        response = requests.get(mURL)
        if (response.status_code != 200):
            raise Exception(
                "Request returned an error: {} {}".format(
                    response.status_code, response.text
                )
            )

        response = response.json()
        result = {
            "num_followers": response["followers_count"], "num_toots": response["statuses_count"]}
        # print(result)
        with open(Path.joinpath(date_dir, "mastodon/mastdon_stats.json"), "w") as f:
            json.dump(result, f)
    except Exception as r:
        logging.error("Exception at get_mastodon_stats()",exc_info=True)

def get_posts(date_dir):
    try:
        f = open(Path.joinpath(parent_dir, "metadata/mastodon_config.json"), "r+")
        fields = json.loads(f.read())
        since = fields["since_id"]
        for i in fields["account_ids"]:
            url = "https://mastodon.technology/api/v1/accounts/{}/statuses".format(i)
            res = requests.get(url, headers={"User-Agent":"kde app"})
            
            if (res.status_code != 200):
                raise Exception("Request returned an error: {} {}".format(res.status_code, res.text))
            data = []
            ids = []
            res = res.json()
            for x in res:
                ids.append(x["id"])
            try:
                last = ids.index(since)
                res = res[0:last]
            except ValueError:
                logging.debug("Last toot not found in list")
            for j in res:
                d = {}
                d["text"] = j["content"]
                d["url"] = j["url"]
                d["id"] = j["id"]
                d["replies_count"] = j["replies_count"]
                d["reblogs_count"] = j["reblogs_count"]
                d["favourites_count"] = j["favourites_count"]
                data.append(d)
            with open(Path.joinpath(date_dir, "mastodon/mastodon_posts.json"),"w") as fpt:
                json.dump(data,fpt)

    except Exception as e:
        logging.error("Exception at get_posts()",exc_info=True)

def main(date_dir):
    path = Path.joinpath(parent_dir, "logs/logs.log")
    logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    try:
        get_mastodon_stats(date_dir)
        get_posts(date_dir)
    except Exception as e:
        logging.error("mastodon", exc_info=True)

# if __name__ == "__main__":
#     get_posts()
#     get_mastodon_stats()
