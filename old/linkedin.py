import requests
import os
import datetime
from decouple import config
import json

# same format as the one that already exists
# https://share.kde.org/s/ItlX9FfMunCADO5?path=%2FSocial%20Media%2FLinkedIn

fname = "data/likedin/aggregated/{}.csv".format(datetime.datetime.now().year)

def check_files():
    #create a metrics file once every year. Metrics file will contain aggregated like count, ... for all the posts until now.
    #So, csv file with 53 rows. 7 columns
    if (not os.path.isfile(fname)):
        f = open(fname, "w")
        f.write("date,uniqueImpressionsCount,shareCount,shareMentionsCount,engagement,clickCount,likeCount,impressionCount,commentMentionsCount,commentCount")
        f.close()
        print("{} File Created".format(fname))
    
def get_aggregated_metrics():
    url = "https://api.linkedin.com/v2/organizationalEntityShareStatistics?q=organizationalEntity&organizationalEntity=urn:li:organization:{}".format(config("LINKEDIN_MY_ORGID"))

    headers = {
        'Authorization': 'Bearer {}'.format(config("linkedin_access_token"))
    }

    response = requests.request("GET", url, headers=headers)

    # if (response.status_code != 200):
    #     raise Exception("Request Failed {} {}".format(response.status_code, response.text))
    # else:
    #     try:
    #         response = response.json()
    #         response = response["elements"][0]["totalShareStatistics"]
    #         df = pd.read_csv(fname)
    #         response["date"] = datetime.datetime.now().strftime("%x")
    #         new_df = pd.DataFrame([response.values()], columns=response.keys())
    #         df = df.append(new_df, ignore_index=True, verify_integrity=True)
    #         df.to_csv(fname, mode="w",index=False, header=True, line_terminator="\n")
    #     except Exception as e:
    #         print("Error while storing likedin aggregate data")
    #         print(e)
    a = response.json()["elements"][0]["totalShareStatistics"]
    with open("linkedin_agg_{}.json".format(datetime.datetime.now()), "w") as ft:
        json.dump(a,ft)

if __name__ == "__main__":
    # check_files()
    get_aggregated_metrics()