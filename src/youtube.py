from googleapiclient.discovery import build
import json
from pathlib import Path
import logging
from decouple import config
import  utils.sentiment as sentiment

parent_dir = Path(__file__).parent.parent.absolute()

youTubeApiKey=config("GOOGLE_API_KEY")
youtube=build('youtube','v3',developerKey=youTubeApiKey)
# channel = "UCF3I1gf7GcbmAb0mR6vxkZQ"

def get_comments(vid_id):
    coms = []
    cd = youtube.commentThreads().list(part="snippet", videoId=vid_id).execute()
    if (len(cd["items"]) ==0 ):
        return coms
    next_token = None
    while 1:
        cd = youtube.commentThreads().list(part="snippet", videoId=vid_id,pageToken=next_token).execute()
        try:
            next_token = cd["nextPageToken"]
        except KeyError:
            for i in cd["items"]:
                c = {}
                c["text"] = i["snippet"]["topLevelComment"]["snippet"]["textDisplay"]
                c["likes"] = i["snippet"]["topLevelComment"]["snippet"]["likeCount"]
                c["id"] = i["snippet"]["topLevelComment"]["id"]
                coms.append(c)
            break
        for i in cd["items"]:
                c = {}
                c["text"] = i["snippet"]["topLevelComment"]["snippet"]["textDisplay"]
                c["likes"] = i["snippet"]["topLevelComment"]["snippet"]["likeCount"]
                c["id"] = i["snippet"]["topLevelComment"]["id"]
                c["sentiment"] = sentiment.sentiment_analysis(c["text"])
                coms.append(c)
    return coms

def get_channel_stats(channel_id,date_dir):
    try:
        #get channel name
        name = youtube.channels().list(part='snippet',id=channel_id).execute()["items"][0]["snippet"]["title"]
        name = name.replace(" ", "-")
        #stats
        statdata=youtube.channels().list(part='statistics',id=channel_id).execute()
        stats = statdata["items"][0]["statistics"]
        with open(Path.joinpath(date_dir, "youtube/{}-channel_stats.json".format(name)), "w") as fpt:
            json.dump(stats, fpt)
    except Exception as e:
        logging.error("youtube.get_channel_stats", exc_info=True)

def get_content_stats(channel_id,last_retreived, date_dir):
    try:

        name = youtube.channels().list(part='snippet',id=channel_id).execute()["items"][0]["snippet"]["title"]
        name = name.replace(" ", "-")

        contentdata=youtube.channels().list(id=channel_id,part='contentDetails').execute()
        uploads  = contentdata['items'][0]['contentDetails']['relatedPlaylists']['uploads']
        videos = []
        if (last_retreived == None):
            res = youtube.playlistItems().list(playlistId=uploads,part='snippet',maxResults=50).execute()
            videos += res["items"]
            video_ids = list(map(lambda x:x['snippet']['resourceId']['videoId'], videos))
        else:
            next_token = None
            res = youtube.playlistItems().list(playlistId=uploads,
                                                part='snippet',
                                                maxResults=50,
                                                pageToken=next_token).execute()
            videos += res['items']
            video_ids = list(map(lambda x:x['snippet']['resourceId']['videoId'], videos))
            if last_retreived in video_ids:
                li = video_ids.index(last_retreived)
                video_ids = video_ids[0:li]

        # video_ids = list(map(lambda x:x['snippet']['resourceId']['videoId'], videos))
        stats = []
        for i in video_ids:
            d = youtube.videos().list(id=i,part="statistics").execute()["items"][0]["statistics"]
            d["id"] = i
            d["comments"] = get_comments(i)
            stats.append(d)
        with open(Path.joinpath(date_dir, "youtube/{}-content_stats.json".format(name)), "w") as fpt:
            json.dump(stats, fpt)

        if len(video_ids) == 0:
            return last_retreived
        else:
            return stats[0]["id"]
    except Exception as e:
        logging.error("youtube.get_channel_stats", exc_info=True)

def main(date_dir):
    path = Path.joinpath(parent_dir, "logs/logs.log")
    logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    try:

        config_file = open(Path.joinpath(parent_dir, "metadata/youtube-config.json"),"r+")
        fields = json.loads(config_file.read())
        # last = fields["lastRetreived"]
        channels = []
        for i in fields["channels"]:
            get_channel_stats(i[0], date_dir)
            last = get_content_stats(i[0], i[1], date_dir)
            i[1] = last
            channels.append(i)
        fields["channels"] = channels
        config_file.seek(0)
        json.dump(fields, config_file)
        config_file.truncate()
        config_file.close()
    except Exception as e:
        logging.error("youtube", exc_info=True)

# if __name__ == "__main__":
#     get_channel_stats("UCF3I1gf7GcbmAb0mR6vxkZQ")
#     get_content_stats(channel, None)