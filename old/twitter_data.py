import requests
import os
import sys
import datetime
import time
import json

#Get all tweets pulished in a week. For all tweets store public_metrics in json file. Excluding retweets and replies. 

def check_files():
    if (not os.path.isdir("data")):
        try:
            os.mkdir("data")
        except OSError:
            sys.exit("Directory creation error")
    
    if (not os.path.isdir("data/twitter")):
        try:
            os.mkdir("data/twitter")
            print("Twitter Directory Created")
        except OSError:
            sys.exit("Directory creation error")

def get_private_data():
    end_time = datetime.datetime.now().replace(microsecond=0).isoformat()
    start_time = datetime.datetime.now() - datetime.timedelta(days=7)
    start_time = start_time.replace(microsecond=0).isoformat()

    url = "https://api.twitter.com/2/users/2988611984/tweets?tweet.fields=attachments,context_annotations,conversation_id,public_metrics,referenced_tweets,text,non_public_metrics,organic_metrics&since_id=1273871467676151808&exclude=retweets"
    # print(url)

    payload={}
#     headers = {
#   'Authorization': 'OAuth oauth_consumer_key="JPxvVqly3jutCmrBaYVB2onPy",oauth_token="2988611984-5ydWvdLFxs1BPfQ2vEmaSHI313vY1se3FWMufoJ",oauth_signature_method="HMAC-SHA1",oauth_timestamp="1611727392",oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",oauth_version="1.0",oauth_callback="http%3A%2F%2F127.0.0.1%2Fcallback",oauth_verifier="AoJZRP24z0LdhqtFBrVRut6Byk4fhBxv",oauth_signature="6ryydqoQFz%2B6sTK%2BhA0kYLyQMRM%3D"'
# }
    headers = {
        'Authorization': 'OAuth oauth_consumer_key="JPxvVqly3jutCmrBaYVB2onPy",oauth_token="2988611984-5ydWvdLFxs1BPfQ2vEmaSHI313vY1se3FWMufoJ",oauth_signature_method="HMAC-SHA1",oauth_timestamp="1611840919",oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",oauth_version="1.0",oauth_callback="http%3A%2F%2F127.0.0.1%2Fcallback",oauth_verifier="AoJZRP24z0LdhqtFBrVRut6Byk4fhBxv",oauth_signature="iNZDlETyDAo3aU2NoZA6n2yNt1I="'
}
    # print(int(time.time()) - 1)

    response = requests.request("GET", url, headers=headers, data=payload)
    
    if (response.status_code != 200):
        print("Error Occured While Sending request. {}. {}".format(response.status_code, response.json()["detail"]))
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )
    else:
        return response.json()

#TODO

def get_mentions_timeline():
    pass

#TODO

def get_replies_to_tweets():
    # get mentions timeline, search for in_reply_to, match the id.
    pass

if __name__ == "__main__":
    with open("data/twitter/{}.json".format(datetime.datetime.now().date()) , "w" ) as f:
        a = get_private_data()
        json.dump(a, f)
