## Reddit

### reddit_config.json

#### subreddit stats

- fields: all the fields to be stored.
- to add subreddits add the names to the array.

#### keyword_search

- keyword_search: searches all subreddits for key words in the array.
- duration: Can be one of: all, day, hour, month, week, year (default: all)
- sort – Can be one of: relevance, hot, top, new, comments. (default: relevance)
- max_posts - limit (default = 100)

#### for retreiving top posts

- names of subreddits
- time = week, day, month, all
- 


## data from instagram, linkedin, facebook is private so only availble for kde page.

## Youtube

for getting channel id, go to the channel home page, and get the id from the url.

Ex: https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ id is UCF3I1gf7GcbmAb0mR6vxkZQ for kdecommunity channel

https://www.youtube.com/channel/UCkIccKaHDGA8lYVmUerLhag for krita channel