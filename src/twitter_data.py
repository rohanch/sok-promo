import requests
import json
import logging
from pathlib import Path
from decouple import config
from requests_oauthlib import OAuth1
import utils.sentiment as sentiment
import datetime
import  utils.sentiment as sentiment

parent_dir = Path(__file__).parent.parent.absolute()

def get_stats(date_dir):
    #general_purpose
    try:
        config_file = open(Path.joinpath(parent_dir, "metadata/twitter_config.json"),"r")
        fields = json.loads(config_file.read())
        config_file.close()
        names = fields['account_statistics']
        for i in names:
            url = "https://api.twitter.com/2/users/by?usernames={}&user.fields=public_metrics".format(i)
            headers = {
                "Authorization": "Bearer {}".format(config("TWITTER_BEARER_TOKEN"))
            }
            res = requests.get(url, headers=headers)
            if (res.status_code != 200):
                raise Exception("API returned error {} {}".format(res.status_code,res.text))

            data = res.json()["data"][0]["public_metrics"]
            with open(Path.joinpath(date_dir, "twitter/{}_twitter_stats.json".format(i)),"w") as fpt:
                json.dump(data, fpt)
    except Exception as e:
        logging.error("Exception",exc_info=True)


def get_mentions(date_dir):
    #general purpose
    try:
        headers = {
            "Authorization": "Bearer {}".format(config("TWITTER_BEARER_TOKEN"))
        }
        f = open(Path.joinpath(parent_dir, "metadata/twitter_config.json"),"r+")
        fields = json.loads(f.read())
        if fields["mentions_since_id"] == None:
            url = "https://api.twitter.com/2/tweets/search/recent?query=to:kdecommunity -is:retweet&tweet.fields=public_metrics,text,id&max_results=100"
            res = requests.get(url, headers=headers)
        else:
            url = "https://api.twitter.com/2/tweets/search/recent?query=to:kdecommunity -is:retweet&since_id={}&tweet.fields=public_metrics,text,id&max_results=100".format(fields["mentions_since_id"])
            res = requests.request("GET", url, headers=headers)
        if res.status_code == 400:
            url = "https://api.twitter.com/2/tweets/search/recent?query=to:kdecommunity -is:retweet&tweet.fields=public_metrics,text,id&max_results=100"
            res = requests.get(url=url, headers=headers)
        if res.status_code != 200:
            raise Exception("Exception {} {}".format(res.status_code,res.text))
        
        res = res.json()
        mentions_file = open(Path.joinpath(date_dir, "twitter/mentions.json"),"w")
        if (res["meta"]["result_count"] == 0):
            json.dump({},mentions_file)
        else:
            json.dump(res["data"],mentions_file)
            #update since_id
            fields["mentions_since_id"] = res["data"][0]["id"]
            f.seek(0)
            json.dump(fields,f)
            f.truncate()
            f.close()
    except Exception as e:
        logging.error("Exception",exc_info=True)
        
def kde_tweets_data(date_dir):
    #works only with @kdecommunity account. 
    try:
        headers = {
            "Authorization": "Bearer {}".format(config("TWITTER_BEARER_TOKEN"))
        }
        oauth = OAuth1(config("TWITTER_API_KEY"), config("TWITTER_API_SECRET_KEY"),config("TWITTER_ACCESS_TOKEN_KDE"),config("TWITTER_ACCESS_TOKEN_SECRET_KDE"))

        config_file = open(Path.joinpath(parent_dir, "metadata/twitter_config.json"),"r+")
        fields = json.loads(config_file.read())
        tweets_since_id = fields["tweets_since_id"]
        if (tweets_since_id != None):
            url = "https://api.twitter.com/2/users/{}/tweets?tweet.fields=attachments,context_annotations,conversation_id,public_metrics,non_public_metrics,referenced_tweets,text&max_results=25&since_id={}".format(config("KDECOMMUNITY_TWITTER_ID"),tweets_since_id)
        else:
            # end_time = datetime.datetime.now().replace(microsecond=0).isoformat()
            # start_time = datetime.datetime.now() - datetime.timedelta(days=7)
            # start_time = start_time.replace(microsecond=0).isoformat()
            # url = "https://api.twitter.com/2/users/{}/tweets?&tweet.fields=attachments,context_annotations,conversation_id,public_metrics,non_public_metrics,referenced_tweets,text&start_time={}Z&end_time={}Z&max_results=100".format(config("KDECOMMUNITY_TWITTER_ID"),start_time,end_time)
            url = "https://api.twitter.com/2/users/{}/tweets?tweet.fields=attachments,context_annotations,conversation_id,public_metrics,non_public_metrics,referenced_tweets,text&max_results=25".format(config("KDECOMMUNITY_TWITTER_ID"))
            
        res = requests.get(url=url, auth=oauth)
        if res.status_code != 200:
            raise Exception("Exception {} {}".format(res.status_code,res.text))
        
        if res.json()["meta"]["result_count"] == 0:
            fl = open(Path.joinpath(date_dir, "twitter/twitter_posts.json"),"w")
            json.dump({} ,fl, indent=4, sort_keys=True)
            logging.info("Twitter Post Data Written to File")
            return

        data = res.json()["data"]

        fields["tweets_since_id"] = data[0]["id"]
        
        config_file.seek(0)
        json.dump(fields,config_file)
        config_file.truncate()
        config_file.close()

        url = "https://api.twitter.com/2/tweets/search/recent?query=to:kdecommunity&tweet.fields=referenced_tweets&max_results=100"
        res = requests.get(url, headers=headers)
        if (res.status_code != 200):
            raise Exception("Exception {} {}".format(res.status_code,res.text))
        replies = res.json()["data"]
        for i in data:
            tweet_id = i["id"]
            coms = []

            for j in replies:
                c = {}
                try:
                    if j["referenced_tweets"][0]["id"] == i["id"]:
                        c["text"] = j["text"]
                        c["id"] = j["id"]
                        c["sentiment"] = sentiment.sentiment_analysis(c["text"])
                        coms.append(c)
                except KeyError:
                    continue
            i["replies"] = coms

        fl = open(Path.joinpath(date_dir, "twitter/twitter_posts.json"),"w")
        json.dump(data ,fl, indent=4, sort_keys=True)
        logging.info("Twitter Post Data Written to File")
    except Exception as e:
        logging.error("Exception",exc_info=True)
    # url = "https://api.twitter.com/2/users/{}/tweets?tweet.fields=attachments,context_annotations,conversation_id,public_metrics,non_public_metrics,referenced_tweets,text&max_results=5".format(config("KDECOMMUNITY_TWITTER_ID"))
    # print(url)
    # res = requests.get(url=url, auth=oauth)
    # res = requests.get(url=url, headers=headers)

    # print(res.json())

def get_public_metrics(date_dir):
    try:
        headers = {
            "Authorization": "Bearer {}".format(config("TWITTER_BEARER_TOKEN"))
        }
        config_file = open(Path.joinpath(parent_dir, "metadata/twitter_config.json"),"r")
        fields = json.loads(config_file.read())
        config_file.close()
        usernames = fields["public_metrics_only"]["usernames"]
        ids = []
        end_time = datetime.datetime.now().replace(microsecond=0).isoformat()
        start_time = datetime.datetime.now() - datetime.timedelta(days=7)
        start_time = start_time.replace(microsecond=0).isoformat()
        for name in usernames:
            url = "https://api.twitter.com/2/users/by/username/{}".format(name)
            res = requests.get(url, headers=headers)
            if (res.status_code != 200):
                raise Exception("API returned error {} {}".format(res.status_code,res.text))
            # ids.append(res.json()["data"]["id"])
            userid = res.json()['data']['id']
            url = "https://api.twitter.com/2/users/{}/tweets?&tweet.fields=attachments,context_annotations,conversation_id,public_metrics,text&start_time={}Z&end_time={}Z&max_results=100".format(userid,start_time,end_time)
            res = requests.get(url, headers=headers)
            if (res.status_code != 200):
                raise Exception("API returned error {} {}".format(res.status_code,res.text))
            data = res.json()
            if data["meta"]["result_count"] == 0:
                data = {}
            else:
                data = data["data"]
            with open(Path.joinpath(date_dir, "twitter/{}_post_metrics.json".format(name) ) , "w") as fpt:
                json.dump(data, fpt)
    except Exception as e:
        logging.error("twitter.get_public_metrics", exc_info=True)

def search_tweets(date_dir):
    try:
        config_file = open(Path.joinpath(parent_dir, "metadata/twitter_config.json"),"r")
        fields = json.loads(config_file.read())
        config_file.close()
        fields = fields["for_gathering_tweets"]
        query = ""
        url = "https://api.twitter.com/2/tweets/search/recent?query="+query
        headers = {
            "Authorization": "Bearer {}".format(config("TWITTER_BEARER_TOKEN"))
        }
        res = requests.get(url, headers=headers)
    except Exception as e:
        logging.error("twitter.search_tweets", exc_info=True)

def main(date_dir):
    path = Path.joinpath(parent_dir, "logs/logs.log")
    logging.basicConfig(filename=path, filemode="a",format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
    try:
        kde_tweets_data(date_dir)
        get_mentions(date_dir)
        get_stats(date_dir)
        get_public_metrics(date_dir)
    except Exception as r:
        logging.error("twitter",exc_info=True)
    # search_tweets()
    

if __name__ == "__main__":
    get_public_metrics(Path("data2"))
